/**
 * Created by Alex Bol on 2/19/2017.
 */

/**
 * Class of system errors
 */
class Errors {
    /**无法从给定参数实例化时引发错误ILLEGAL_PARAMETERS
     * Throw error ILLEGAL_PARAMETERS when cannot instantiate from given parameter
     * @returns {ReferenceError}
     *
     * ILLEGAL 非法的
     *
     */
    static get ILLEGAL_PARAMETERS() {
        return new ReferenceError('Illegal Parameters');
    }

    /**抛出错误ZERO_DIVISION以捕捉零除法的情况
     * Throw error ZERO_DIVISION to catch situation of zero division
     * @returns {Error}
     */
    static get ZERO_DIVISION() {
        return new Error('Zero division');
    }

    /**当fixBoundaryConflicts无法修复时，从BooleanOperations模块抛出错误
     * Error to throw from BooleanOperations module in case when fixBoundaryConflicts not capable to fix it
     * @returns {Error}
     */
    static get UNRESOLVED_BOUNDARY_CONFLICT() {
        return new Error('Unresolved boundary conflict in boolean operation');
    }

    /**
     * Error to throw from LinkedList:testInfiniteLoop static method 从LinkedList引发错误：testInfiniteLoop静态方法
     * in case when circular loop detected in linked list 在链表中检测到循环时
     * @returns {Error}
     */
    static get INFINITE_LOOP() {
        return new Error('Infinite loop');
    }

    static get CANNOT_INVOKE_ABSTRACT_METHOD() {
        return new Error('Abstract method cannot be invoked 无法调用抽象方法');
    }

    // 不支持操作
    static get OPERATION_IS_NOT_SUPPORTED() {
        return new Error('Operation is not supported')
    }
}

export default Errors;
