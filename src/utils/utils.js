/**
 * Created by Alex Bol on 2/18/2017.
 */

/**
 * Floating point comparison tolerance.
 * Default value is 0.000001 (10e-6)
 * @type {number}
 */
let DP_TOL = 0.000001;

/**
 * Set new floating point comparison tolerance
 * @param {number} tolerance
 */
export function setTolerance(tolerance) {DP_TOL = tolerance;}

/**
 * Get floating point comparison tolerance
 * @returns {number}
 */
export function getTolerance() {return DP_TOL;}

//DECIMALS 小数
export const DECIMALS = 3;

/**
 * 如果值与零相当，则返回*true*
 * Returns *true* if value comparable to zero
 * @param {number} x
 * @param {number} y
 * @return {boolean}
 */
export function EQ_0(x) {
    return (x < DP_TOL && x > -DP_TOL);
}

/**
 * 如果两个值等于DP_TOL，则返回*true*
 * Returns *true* if two values are equal up to DP_TOL
 * @param {number} x
 * @param {number} y
 * @return {boolean}
 */
export function EQ(x, y) {
    return (x - y < DP_TOL && x - y > -DP_TOL);
}

/**
 * 如果DP_TOL之前第一个参数大于第二个参数，则返回*true*
 * Returns *true* if first argument greater than second argument up to DP_TOL
 * @param {number} x
 * @param {number} y
 * @return {boolean}
 * greater than 大于
 * greeter 更大
 */
export function GT(x, y) {
    return (x - y > DP_TOL);
}

/**
 * 如果第一个参数大于或等于DP_TOL之前的第二个参数，则返回*true*
 * Returns *true* if first argument greater than or equal to second argument up to DP_TOL
 * @param {number} x
 * @param {number} y
 * @returns {boolean}
 *
 * Greeter Equal
 *
 */
export function GE(x, y) {
    return (x - y > -DP_TOL);
}

/**
 * 如果第一个参数小于第二个参数，则返回*true*直到DP_TOL
 * Returns *true* if first argument less than second argument up to DP_TOL
 * @param {number} x
 * @param {number} y
 * @return {boolean}
 *
 * less then 小于
 */
export function LT(x, y) {
    return (x - y < -DP_TOL)
}

/**
 * 如果第一个参数小于或等于DP_TOL之前的第二个参数，则返回*true*
 * Returns *true* if first argument less than or equal to second argument up to DP_TOL
 * @param {number} x
 * @param {number} y
 * @return {boolean}
 */
export function LE(x, y) {
    return (x - y < DP_TOL);
}
